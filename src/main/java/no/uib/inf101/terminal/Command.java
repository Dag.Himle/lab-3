package no.uib.inf101.terminal;

import java.util.Map;

public interface Command {

    /**
     * Runs the command with the given arguments.
     * 
     * @param args
     *            The arguments to the command
     * @return The output of the command
     */
    String run(String[] args);


    /**
     * Get the name of the command.
     * 
     * @return The name of the command
     */
    String getName();

    /**
     * Set the context of the command.
     * 
     * @param context
     *            The context of the command
     */
    default void setContext(Context context) { /* do nothing */ };

    /**
     * Get the help text of the command.
     * 
     * @return The help text of the command
     */
    String getHelp();

    /**
     * Set the context of the command.
     * 
     * @param context
     *            The context of the command
     */
    default void setCommandContext(Map<String, Command> commands) { /* do nothing */ };
    
}
