package no.uib.inf101.terminal;

import java.util.Map;

public class CmdHelp implements Command{

    Map<String, Command> commands;

    @Override
    public String run(String[] args) {
        String result = "";
        if (args.length == 0) {
            for (Command cmd : commands.values()) {
                result += cmd.getHelp() + "\n";
            }
            return result;
        }
        for (String arg : args) {
            if (commands.containsKey(arg)) {
                result += commands.get(arg).getHelp() + "\n";
            } else { 
                result += "No such command: " + arg;
            }
        }
        return result;
    }

    @Override
    public String getName() {
        return "help";
    }   

    @Override
    public String getHelp() {
        return "help: print help text for a command";
    }

    @Override
    public void setCommandContext(Map<String, Command> commands) {
        this.commands = commands;
    }

    
}
